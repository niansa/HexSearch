#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>



struct File {
    int fd = -1;
    void *data = MAP_FAILED;
    size_t size;
    const char *name;

    File(const char *file_name) : name(file_name) {
        // Open file
        fd = open(file_name, O_RDONLY);
        if (fd < 0) {
            perror("Failed to open file");
            abort();
        }

        // Get file size
        {
            struct stat file_stat;
            fstat(fd, &file_stat);
            size = file_stat.st_size;
        }

        // Map file to memory
        data = mmap(NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
        if (data == MAP_FAILED) {
            perror("Failed to map file to memory");
            abort();
        }
    }

    ~File() {
        if (data != MAP_FAILED) {
            munmap(data, size);
        }
        if (fd >= 0) {
            close(fd);
        }
    }
};


template<typename T>
off_t find_value(const File& file, T value) {
    for (off_t idx = 0; idx < file.size - sizeof(value); idx++) {
        if (*reinterpret_cast<T*>(reinterpret_cast<int8_t*>(file.data) + idx) == value) {
            return idx;
        }
    }
    return -1;
}

template<typename T>
T atoai(const char *str) {
    T fres = 0;
    bool negate = false;
    // Iterate through string
    for (const char *c = str; *c != '\0'; c++) {
        if (*c == '-') {
            negate = true;
        } else {
            fres += *c - '0';
            fres *= 10;
        }
    }
    fres /= 10;
    // Negate
    if (negate) {
        fres = -fres;
    }
    // Return result
    return fres;
}


int main(int argc, const char **argv) {
    // Args
    if (argc < 4) {
        printf("Usage: %s <file> <type> <data>", argv[0]);
        return -1;
    }
    const char *type = argv[2],
               *data = argv[3];

    // Open file
    File file(argv[1]);

    // Search for value
    off_t loc = -1;
    if (strcmp(type, "8") == 0) {
        loc = find_value<int8_t>(file, atoai<int8_t>(data));
    } else if (strcmp(type, "16") == 0) {
        loc = find_value<int16_t>(file, atoai<int16_t>(data));
    } else if (strcmp(type, "32") == 0) {
        loc = find_value<int32_t>(file, atoai<int32_t>(data));
    } else if (strcmp(type, "64") == 0) {
        loc = find_value<int64_t>(file, atoai<int64_t>(data));
    } else if (strcmp(type, "u8") == 0) {
        loc = find_value<uint8_t>(file, atoai<uint8_t>(data));
    } else if (strcmp(type, "u16") == 0) {
        loc = find_value<uint16_t>(file, atoai<uint16_t>(data));
    } else if (strcmp(type, "u32") == 0) {
        loc = find_value<uint32_t>(file, atoai<uint32_t>(data));
    } else if (strcmp(type, "u64") == 0) {
        loc = find_value<uint64_t>(file, atoai<uint64_t>(data));
    } else if (strcmp(type, "float") == 0) {
        loc = find_value<float>(file, atoai<float>(data));
    } else if (strcmp(type, "double") == 0) {
        loc = find_value<double>(file, atoai<double>(data));
    } else {
        printf("Type is unknown\n");
    }

    if (loc < 0) {
        printf("Data not found\n");
    } else {
        printf("Data found at %p\n", reinterpret_cast<void*>(loc));
    }
}
